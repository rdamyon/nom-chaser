# README #

Source Code for the NomChaser Android App and AppEngine backend code

### What is this repository for? ###

* Nom Chaser tracks food trucks locations around Australia by polling their twitter feeds periodically and serving this data up via a number of interfaces.
* Current implementation includes an Android application with the ability to store and update favourite trucks. Web interfaces utilising AngualrJS and React are planned.  
* Version

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Any questions contact rdamyon@gmail.com