package com.rdamyon.nomchaser.net;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import com.rdamyon.nomchaser.R;
import com.rdamyon.nomchaser.activity.NomChaserActivity;
import com.rdamyon.nomchaser.config.Config;
import com.rdamyon.nomchaser.service.TruckService;
import com.rdamyon.nomchaser.service.impl.TruckServiceImpl;
import com.rdamyon.nomchaser.model.Truck;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Async task to fetch the latest set of truck tweets from the remote server
 */
public class TruckListRequestTask extends AsyncTask<String, Boolean, Boolean> {

	private static final String SERVER_ADDRESS = "http://nomchaser.appspot.com";
	private static final String TIMELINE_PATH = "/timelines";
    private static final int MINUTES_BETWEEN_UPDATES = 1;

	private Context context;
    private NomChaserActivity activity;
    private ProgressDialog pd;
    private SharedPreferences settings;
	
	public TruckListRequestTask(Context context, NomChaserActivity activity)
	{
		this.context = context;
        this.activity = activity;
        this.settings =  context.getSharedPreferences(context.getString(R.string.shared_prefs_filename), Context.MODE_PRIVATE);
	}
	
	protected Boolean doInBackground(String... strings)
	{
		int count = strings.length;
        long lastUpdate = settings.getLong(context.getString(R.string.last_update_pref_name), 0);
        long currentDate = (new Date()).getTime();
        //Check if minimum time to update has passed
        if((currentDate - lastUpdate) > (MINUTES_BETWEEN_UPDATES * 1000 * 60)){
            try{
            if(count > 0)
            {
                // TODO Implement retrieving individual truck timelines
                getLatestTruckList();
            }else
            {
                getLatestTruckList();
            }
            }catch(Exception e)
            {
                Log.e(this.getClass().toString(), e.getMessage() == null ? e.getMessage() : e.toString());
                return false;
            }
        }
		return true;
	}

    protected void onPreExecute()
    {

        //pd=ProgressDialog.show(activity, "", "", false);
    }

	protected void onPostExecute(Boolean result)
	{
        //pd.dismiss();
        activity.refreshCompleted();
	}

    @Override
    protected void onCancelled() {
        super.onCancelled();
        activity.refreshCompleted();
        //pd.cancel();
    }

    private void getLatestTruckList() throws IOException
	{
		URL url = new URL(SERVER_ADDRESS + TIMELINE_PATH);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setUseCaches(false);
        InputStream in = new BufferedInputStream(con.getInputStream());
        parseTruckList(in);
	}
	
	private void parseTruckList(InputStream in)
	{
		JsonParser parser = new JsonParser();
		Gson gson = new Gson();
		TruckService truckService = TruckServiceImpl.getInstance(this.context);
		Reader reader = new InputStreamReader(in);
		JsonArray jsonArray = parser.parse(reader).getAsJsonArray();
		if(jsonArray.isJsonArray() && jsonArray.size() > 0)
		{
            int latestHashValue = jsonArray.toString().hashCode();

            Integer currentHashValue = settings.getInt(context.getString(R.string.latest_hash_pref_name), 0);
            //If the hash of the json payload matches the latest stored result then no data has changed.
            if(currentHashValue == 0 || !currentHashValue.equals(latestHashValue))
            {
                List<Truck> truckList = new ArrayList<Truck>();
                for(int i = 0; i < jsonArray.size(); i++)
                {
                    JsonElement jsonTruck = jsonArray.get(i);
                    truckList.add(gson.fromJson(jsonTruck, Truck.class));
                }
                truckService.updateTruckList(truckList);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt(context.getString(R.string.latest_hash_pref_name), 0);
                editor.putLong(context.getString(R.string.last_update_pref_name), (new Date()).getTime());
                editor.commit();
            }
		}
	}
}
