package com.rdamyon.nomchaser.activity;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.view.*;
import android.widget.*;
import com.rdamyon.nomchaser.R;
import com.rdamyon.nomchaser.config.Config;
import com.rdamyon.nomchaser.dialog.InfoDialog;
import com.rdamyon.nomchaser.model.TruckType;
import com.rdamyon.nomchaser.net.TruckListRequestTask;
import android.content.Intent;
import android.os.Bundle;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity implements NomChaserActivity {

    TruckListRequestTask task;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refresh();
	}

    @Override
    public void onPause() {
        super.onPause();
        if (task != null)
        {
            task.cancel(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_nom_chaser_main, menu);

        return true;
    }

    public void refreshCompleted(){
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.main_layout_view);

        linearLayout.removeAllViews();
        List<TruckType> truckTypeList = Arrays.asList(TruckType.values());

        for (TruckType truckType : truckTypeList) {
            TextView textView = (TextView) getLayoutInflater().inflate(R.layout.main_option, null);
            textView.setText(truckType.getDisplayName());
            textView.setTag(truckType);
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchActivity(view);
                }
            });
            linearLayout.addView(textView);
        }
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
    }

    public void launchActivity(View view){
        if (TruckType.FAVOURITE.equals(view.getTag())){
            launchFavouriteActivity(view);
        }else{
            launchCityActivity(view);
        }
    }

	public void launchFavouriteActivity(View view)
	{
		Intent intent = new Intent(this, TruckListActivity.class);
		intent.putExtra(Config.TRUCK_LIST_TYPE_INTENT, (TruckType)view.getTag());
		startActivity(intent);
	}

    public void launchCityActivity(View view)
    {
    	Intent intent = new Intent(this, TruckListActivity.class);
		intent.putExtra(Config.TRUCK_LIST_TYPE_INTENT,(TruckType)view.getTag());
		startActivity(intent);

    }

    public void showInfoDialog(MenuItem item){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("info-dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DialogFragment newFragment = new InfoDialog();
        newFragment.show(ft, "info-dialog");
    }

    public boolean refresh(MenuItem item)
    {
        refresh();
        return true;
    }

    private void refresh(){
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.main_layout_view);
        linearLayout.removeAllViews();

        View progressView = getLayoutInflater().inflate(R.layout.loading_layout, null);
        linearLayout.addView(progressView);

        task = new TruckListRequestTask(getApplicationContext(), this);
        task.execute();
    }


}
