package com.rdamyon.nomchaser.activity;

import java.util.List;

import android.app.Activity;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rdamyon.nomchaser.arrayadapter.TruckListArrayAdapter;
import com.rdamyon.nomchaser.config.Config;
import com.rdamyon.nomchaser.model.TruckType;
import com.rdamyon.nomchaser.service.impl.TruckServiceImpl;
import com.rdamyon.nomchaser.service.TruckService;
import com.rdamyon.nomchaser.model.Truck;
import com.rdamyon.nomchaser.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class TruckListActivity extends Activity implements NomChaserActivity {

	private TruckType type;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_truck);
        ListView listView = (ListView)findViewById(R.id.truck_list_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
                launchTweetListActivity(v,pos,id);
            }
        });

	 	type = (TruckType)this.getIntent().getSerializableExtra(Config.TRUCK_LIST_TYPE_INTENT);
        setTitle(type.getDisplayName());
	 	List<Truck> truckList = getTruckListForType(type);
	 	if(truckList == null || truckList.isEmpty())
	 	{
	 		String text;
	 		if(TruckType.FAVOURITE.getTag().equals(type))
	 		{
	 			text = "Add favourites by hitting the heart on a truck's page.";
	 		}
	 		else{
	 			text = "No trucks found.";
	 		}
	 		Toast toast = Toast.makeText(this.getApplicationContext(), text, Toast.LENGTH_LONG);
			toast.show();
	 	}
        listView.setAdapter(new TruckListArrayAdapter(this, truckList));
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO - Testing for now
        AdView adView = (AdView)this.findViewById(R.id.truck_ad_view);
        AdRequest adRequest = new AdRequest.Builder()
				.addKeyword("food")
				.addKeyword(type.getDisplayName())
				.build();
        adView.loadAd(adRequest);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    	default :
	    		return super.onOptionsItemSelected(item);
    	}
    }

    public void refreshCompleted(){
        //Not implemented in this activity
    }

	private List<Truck> getTruckListForType(TruckType type)
	{
		TruckService truckService = TruckServiceImpl.getInstance(this.getBaseContext());
		List<Truck> truckList;
		if(TruckType.FAVOURITE.equals(type))
		{
			truckList = truckService.getFavouriteTruckList();
		}else
		{
			truckList = truckService.getCityTruckList(type);
				
		}		
		return truckList;
		
	}
	
	protected void launchTweetListActivity(View view, int pos, long id)
	{
		Intent intent = new Intent(this, TweetListActivity.class);
		intent.putExtra(Config.TRUCK_INTENT, (String)view.getTag());
		startActivity(intent);
	}


}
