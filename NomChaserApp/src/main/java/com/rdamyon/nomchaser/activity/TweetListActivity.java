package com.rdamyon.nomchaser.activity;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.rdamyon.nomchaser.net.TruckListRequestTask;
import org.apache.commons.lang.StringUtils;
import com.rdamyon.nomchaser.arrayadapter.TweetListArrayAdapter;
import com.rdamyon.nomchaser.config.Config;
import com.rdamyon.nomchaser.service.TruckService;
import com.rdamyon.nomchaser.service.impl.TruckServiceImpl;
import com.rdamyon.nomchaser.model.Truck;
import com.rdamyon.nomchaser.R;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;

import java.lang.reflect.Field;

public class TweetListActivity extends Activity implements NomChaserActivity {

	private Truck truck;
    TruckListRequestTask task;

    // Maintain twitterId to allow for refresh from db.
    private String twitterId;

    static DisplayImageOptions imageOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisc(true)
            .build();

    static ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tweet);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(imageOptions)
                .build();
        imageLoader.init(config);

        twitterId = (String)this.getIntent().getSerializableExtra(Config.TRUCK_INTENT);
	 	// TODO Can truck ever be null?

        loadTruckData();
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_nom_chaser_tweet_list, menu);
		MenuItem favouriteItem = menu.findItem(R.id.menu_favourite);
		if(truck != null && truck.getFavourite())
		{
			favouriteItem.setIcon(R.drawable.ic_rating_favourite_on);
            favouriteItem.setTitle("Unfavourite");
		}		
	    
	    return true;
	}

    //TODO Find a better way of always enabling the overflow menu
    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.i(this.getClass().toString(), e.getMessage());
        }
    }	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    	default :
	    		return super.onOptionsItemSelected(item);
    	}
    }

    @Override
    public void onPause() {
        super.onPause();
        if (task != null)
        {
            task.cancel(true);
        }
    }
    public boolean toggleFavourite(MenuItem item)
    {
        if(truck != null)
        {
            truck.setFavourite(!truck.getFavourite());
            TruckService truckService = TruckServiceImpl.getInstance(this.getBaseContext());
            truckService.toggleFavourite(truck);
            ActivityCompat.invalidateOptionsMenu(this);
        }
    	return true;
    }

    public boolean launchTwitter(MenuItem item)
    {
    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.twitter.com/" + twitterId));
    	startActivity(browserIntent);
    	return true;
    }

    public boolean refresh(MenuItem item)
    {
        truck = null;
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.tweet_root_layout_view);
        linearLayout.removeAllViews();

        View progressView = getLayoutInflater().inflate(R.layout.loading_layout, null);
        linearLayout.addView(progressView);

        task = new TruckListRequestTask(getApplicationContext(), this);
        task.execute();
        return true;
    }

    /**
     * Called upon successful or failed refresh of the activity.
     */
    public void refreshCompleted(){
        loadTruckData();
    }

    private void loadTruckData(){
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.tweet_root_layout_view);

        linearLayout.removeAllViews();

        View tweetView = getLayoutInflater().inflate(R.layout.tweet_layout, null);
        linearLayout.addView(tweetView);

        TruckService truckService = TruckServiceImpl.getInstance(this.getBaseContext());

        truck = truckService.getTruck(twitterId);

        TextView textView = (TextView)findViewById(R.id.tweet_list_header_text);
        ImageView imageView = (ImageView)findViewById(R.id.tweet_list_header_image);

        if(truck.getUser() != null){

            String imageUrl = truck.getUser().getProfile_image_url();

            // suffixes are _normal (48 x 48) _bigger (73 x 73) or blank for full size
            if(StringUtils.isNotEmpty(imageUrl)){
                imageUrl = imageUrl.replace("_normal", "_bigger");
                imageLoader.displayImage(imageUrl, imageView, imageOptions);
            }

            textView.setText(truck.getUser().getName());
        }

        ListView listView = (ListView)findViewById(R.id.tweet_list_view);
        listView.setAdapter(new TweetListArrayAdapter(this, truck));
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if(currentapiVersion >= 11){
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getOverflowMenu();

        AdView adView = (AdView)this.findViewById(R.id.tweet_ad_view);
        AdRequest adRequest = new AdRequest.Builder()
                .addKeyword("food")
                .addKeyword(truck.getCity())
                .build();
        adView.loadAd(adRequest);
    }
}
