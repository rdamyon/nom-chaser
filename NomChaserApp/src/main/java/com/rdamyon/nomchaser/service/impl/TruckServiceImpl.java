package com.rdamyon.nomchaser.service.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.rdamyon.nomchaser.model.Truck;
import com.rdamyon.nomchaser.model.TruckTweet;
import com.rdamyon.nomchaser.model.TruckType;
import com.rdamyon.nomchaser.model.User;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.rdamyon.nomchaser.service.TruckService;

// TODO Investigate using an ORM instead of manually handling database queries.
public class TruckServiceImpl extends SQLiteOpenHelper implements TruckService{

	private static final String DATABASE_NAME = "nomchaser.db";
	private static final int VERSION_NO = 1;

    private static final String TRUCK_TABLE_NAME = "TRUCKS";

    private static final String COL_TWITTER_ID = "twitterId";
	private static final String COL_CITY = "city";
	private static final String COL_FAVOURITE = "fav";
	private static final String COL_TWEET_LIST = "tweets";
	private static final String COL_USER = "user";
	
	private static TruckServiceImpl instance;
	private static SQLiteDatabase writableDb;
	
	private TruckServiceImpl(Context context) {
		
		super(context, DATABASE_NAME, null, VERSION_NO);
		
	}
	
	public static TruckServiceImpl getInstance(Context context)
	{
		if (instance == null) {
			instance = new TruckServiceImpl(context);
		}
		return instance;
	}

	 @Override
	public void onCreate(SQLiteDatabase db) {
		String createTruckTable = 
			"CREATE TABLE " + TRUCK_TABLE_NAME + " ("
			+ COL_TWITTER_ID + " TEXT PRIMARY KEY NOT NULL, "
			+ COL_CITY + " TEXT NOT NULL, "
			+ COL_FAVOURITE + " INTEGER, "
			+ COL_TWEET_LIST + " TEXT NOT NULL, "
			+ COL_USER + " TEXT NOT NULL "
			+ ");";

		db.execSQL(createTruckTable);

	}
	 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	private SQLiteDatabase getWritableDatabaseInstance() {
		if(writableDb == null || (!writableDb.isOpen()))
		{
			writableDb = this.getWritableDatabase();
		}
		return writableDb;
	}

    @Override
    public void updateTruckList(List<Truck> truckList) {
        SQLiteDatabase db = getWritableDatabaseInstance();

        db.beginTransaction();
        try{
        for(Truck truck : truckList)
        {
            replace(truck, db);
        }
        db.setTransactionSuccessful();
        }catch(SQLException e)
        {
            Log.e(this.getClass().toString(), e.getMessage());
        }finally {
            db.endTransaction();
        }
    }

    private void replace(Truck truck, SQLiteDatabase db) throws SQLException
	{
        Gson gson = new Gson();
        ContentValues values = new ContentValues();
        values.put(COL_TWITTER_ID, truck.getTwitterId());
        values.put(COL_CITY, truck.getCity());
        String tweetList = gson.toJson(truck.getTweets());
        if(tweetList != null)
        {
            values.put(COL_TWEET_LIST, tweetList);
        }
        String user = gson.toJson(truck.getUser());
        if(user != null)
        {			
            values.put(COL_USER, user);
        }

        //Avoid overwriting favourite
        //TODO extract favourites into another table to avoid needing to extract this info from the DB on update.
        Truck persistedTruck = getTruck(truck.getTwitterId());

        if(persistedTruck != null && persistedTruck.getFavourite()){
            values.put(COL_FAVOURITE, true);
        }
        // TODO Use replace here??
        db.replaceOrThrow(TRUCK_TABLE_NAME, null, values);		
	}

    public void toggleFavourite(Truck truck){
        try{
            SQLiteDatabase db = getWritableDatabaseInstance();
            ContentValues values = new ContentValues();
            values.put(COL_FAVOURITE, truck.getFavourite());
            String whereClause = COL_TWITTER_ID  + " = '" + truck.getTwitterId() + "'";
            db.update(TRUCK_TABLE_NAME, values, whereClause, null);
        }catch(SQLException e)
        {
            Log.e(this.getClass().toString(), e.getMessage());
        }
    }

	public List<Truck> getCityTruckList(TruckType type)
	{
		String whereClause = COL_CITY + " = '" + type.getTag() + "'";
		return getTruckList(whereClause);
	}
	
	public List<Truck> getFavouriteTruckList()
	{
		String whereClause = COL_FAVOURITE + " = 1";
		return getTruckList(whereClause);
	}

    public Truck getTruck(String twitterId){
        Truck truck = null;
        try{
        String whereClause = COL_TWITTER_ID + " = '" + twitterId + "'";
        SQLiteDatabase db = getWritableDatabaseInstance();
        Cursor result = db.query(true, TRUCK_TABLE_NAME, null, whereClause, null, null, null, COL_TWITTER_ID, null);
        while(result.moveToNext())
        {
           truck = populateTruck(result);
        }
        }catch(SQLException e)
        {
            Log.e(this.getClass().toString(), e.getMessage());
        }
        return truck;
    }

	private List<Truck> getTruckList(String whereClause)
	{
		List<Truck> truckList = new ArrayList<Truck>();
		try{
			SQLiteDatabase db = getWritableDatabaseInstance();
			Cursor result = db.query(true, TRUCK_TABLE_NAME, null, whereClause, null, null, null, COL_TWITTER_ID, null);
			while(result.moveToNext())
			{
				truckList.add(populateTruck(result));
			}
		}catch(SQLException e)
		{
			Log.e(this.getClass().toString(), e.getMessage());
		}

		return truckList;
	}
	
	@SuppressWarnings("unchecked")
	private Truck populateTruck(Cursor cursor)
	{
		Gson gson = new Gson();
		Truck truck = new Truck();
		truck.setTwitterId(cursor.getString(cursor.getColumnIndex(COL_TWITTER_ID)));
		truck.setCity(cursor.getString(cursor.getColumnIndex(COL_CITY)));
		truck.setFavourite(cursor.getInt(cursor.getColumnIndex(COL_FAVOURITE)) == 1);
		String tweetJson = cursor.getString(cursor.getColumnIndex(COL_TWEET_LIST));
		Type tweetListType = new TypeToken<List<TruckTweet>>(){}.getType();
		truck.setTweets((List<TruckTweet>)gson.fromJson(tweetJson, tweetListType));
		String userJson = cursor.getString(cursor.getColumnIndex(COL_USER));
		truck.setUser(gson.fromJson(userJson, User.class));
		return truck;
	}
}
