package com.rdamyon.nomchaser.service;

import com.rdamyon.nomchaser.model.Truck;
import com.rdamyon.nomchaser.model.TruckType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Rene
 * Date: 27/08/13
 * Time: 8:23 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TruckService {
    
    public void updateTruckList(List<Truck> truckList);

    public void toggleFavourite(Truck truck);

    public List<Truck> getCityTruckList(TruckType type);

    public List<Truck> getFavouriteTruckList();

    public Truck getTruck(String twitterId);
}
