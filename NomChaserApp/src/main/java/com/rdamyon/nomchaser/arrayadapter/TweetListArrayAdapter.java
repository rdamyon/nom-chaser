package com.rdamyon.nomchaser.arrayadapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.rdamyon.nomchaser.R;
import com.rdamyon.nomchaser.model.Truck;
import com.rdamyon.nomchaser.model.TruckTweet;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class TweetListArrayAdapter extends ArrayAdapter<TruckTweet> {
	
	private final Context context;
	private final Truck truck;
	
	final String TWITTER_TIME="EEE MMM dd HH:mm:ss ZZZZZ yyyy";

	public TweetListArrayAdapter(Context context, Truck truck) {
		super(context,R.layout.list_tweet, (truck.getTweets() != null) ? truck.getTweets() : new ArrayList<TruckTweet>());
		if(truck.getTweets() == null || truck.getTweets().isEmpty())
		{
			Toast toast = Toast.makeText(context, "No tweets found for this guy...for some reason.", Toast.LENGTH_SHORT);
			toast.show();
		}
		this.context = context;
		this.truck = truck;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v == null)
		{
			LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.list_tweet, null);
		}	
		TextView tweetView = (TextView)v.findViewById(R.id.tweet);
		TextView timestampView = (TextView)v.findViewById(R.id.timestamp);
		TruckTweet tweet = truck.getTweets().get(position);
		tweetView.setText(tweet.getText());
		try {
			DateFormat dateFormat = new SimpleDateFormat(TWITTER_TIME, Locale.ENGLISH);
			Date timestamp = dateFormat.parse(tweet.getCreated_at());			

			DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy  hh:mm aa");
			timestampView.setText(outputFormat.format(timestamp));
		} catch (ParseException e) {
			Log.w(this.getClass().toString(), e.getMessage());
			timestampView.setText("Unknown time");
		}
		return v;
		
	}
		
	
	

}
