package com.rdamyon.nomchaser.arrayadapter;

import java.util.List;

import android.widget.RelativeLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import com.rdamyon.nomchaser.R;
import com.rdamyon.nomchaser.model.Truck;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TruckListArrayAdapter extends ArrayAdapter<Truck> {
	
	private static final String TAG = "TruckListArrayAdapter";
	
	private final Context context;
	private final List<Truck> objects;
	
	static DisplayImageOptions imageOptions = new DisplayImageOptions.Builder()
    .cacheInMemory(true)
    .cacheOnDisc(true)
    .build();
	
	static ImageLoader imageLoader = ImageLoader.getInstance();		
	
	public TruckListArrayAdapter(Context context, List<Truck> objects) {
		super(context,R.layout.list_truck, objects);
		this.context = context;
		this.objects = objects;
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())
	    .defaultDisplayImageOptions(imageOptions)
	    .build();
		imageLoader.init(config);
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try{
			if(convertView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				
				convertView = inflater.inflate(R.layout.list_truck, parent, false);
			}
            RelativeLayout layout = (RelativeLayout)(convertView.findViewById(R.id.truck_list_layout));
			TextView textView = (TextView)convertView.findViewById(R.id.truck_list_text);
			ImageView imageView = (ImageView)convertView.findViewById(R.id.truck_list_image);
			
			if(position == 0 && objects.isEmpty())
			{
				textView.setText("Here be a truck wasteland, nothing to see for miles around.");
			}else{
				Truck truck = objects.get(position);
                layout.setTag(truck.getTwitterId());
				if(truck.getUser() != null)
				{
					String url = truck.getUser().getProfile_image_url();
					textView.setText(truck.getUser().getName());
					imageLoader.displayImage(url, imageView, imageOptions);
				}else
				{
					// Haven't got data for some reason?? Twitter account deleted or private perhaps.
					Log.w(TAG, "No data found for Truck :" + truck.getTwitterId());
					textView.setText(truck.getTwitterId());
					imageView.setImageDrawable(null);
				}
			}
		}
		catch(Exception e)
		{
			Log.e(TAG, e.getMessage());
		}
		return convertView;
		
		
	}
		
	
	

}
