package com.rdamyon.nomchaser.arrayadapter;

import android.content.Context;
import android.graphics.Point;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.rdamyon.nomchaser.R;
import com.rdamyon.nomchaser.model.TruckType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ReneNew
 * Date: 27/08/13
 * Time: 9:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainArrayAdapter extends ArrayAdapter<TruckType> {

    private final Context context;
    private final List<TruckType> truckTypeList;
    private int height;

    public MainArrayAdapter(Context context, List<TruckType> truckTypeList){
        super(context, R.layout.list_main , truckTypeList);
        this.truckTypeList = truckTypeList;
        this.context = context;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        height = size.y;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_main, null);
        }

        TextView textView = (TextView)convertView.findViewById(R.id.main_list_text);
        //textView.setY(height / getCount());
        convertView.setY(height / getCount());
        TruckType truckType = truckTypeList.get(position);
        textView.setText(truckType.getDisplayName());
        convertView.setTag(truckType.getTag());
        return convertView;
    }
}
