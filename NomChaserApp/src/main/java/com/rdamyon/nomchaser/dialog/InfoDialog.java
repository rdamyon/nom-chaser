package com.rdamyon.nomchaser.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.rdamyon.nomchaser.R;

/**
 * User: ReneNew
 * Date: 9/10/13
 * Time: 7:49 PM
 */
public class InfoDialog extends DialogFragment {

    public InfoDialog() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.app_name);

        TextView v = (TextView)inflater.inflate(R.layout.dialog_info, container, false);

        v.setText("Nom Chaser will keep track of your favourite food trucks around Australia.\n\n" +
            "If you know of any food trucks that aren't on the list email me at rdamyondev@gmail.com or tweet me @nomchaser");

        return v;
    }
}