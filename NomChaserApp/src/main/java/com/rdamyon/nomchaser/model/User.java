
package com.rdamyon.nomchaser.model;

import java.io.Serializable;

public class User implements Serializable
{

	private static final long serialVersionUID = 6517474177647914211L;

	private String profile_text_color;

    private String profile_background_color;

    private String profile_image_url;

    private String screen_name;

    private String name;

    private String utc_offset;

    public User()
    {
    }

    public User(final String profile_text_color, final String profile_background_color, final String profile_image_url,
            final String screen_name, final String name, final String utc_offset)
    {
        this.profile_text_color = profile_text_color;
        this.profile_background_color = profile_background_color;
        this.profile_image_url = profile_image_url;
        this.screen_name = screen_name;
        this.name = name;
        this.utc_offset = utc_offset;
    }

    public String getProfile_text_color()
    {
        return this.profile_text_color;
    }

    public void setProfile_text_color(final String profile_text_color)
    {
        this.profile_text_color = profile_text_color;
    }

    public String getProfile_background_color()
    {
        return this.profile_background_color;
    }

    public void setProfile_background_color(final String profile_background_color)
    {
        this.profile_background_color = profile_background_color;
    }

    public String getProfile_image_url()
    {
        return this.profile_image_url;
    }

    public void setProfile_image_url(final String profile_image_url)
    {
        this.profile_image_url = profile_image_url;
    }

    public String getScreen_name()
    {
        return this.screen_name;
    }

    public void setScreen_name(final String screen_name)
    {
        this.screen_name = screen_name;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setUtc_offset(final String utc_offset)
    {
        this.utc_offset = utc_offset;
    }

    public String getUtc_offset()
    {
        return this.utc_offset;
    }

}
