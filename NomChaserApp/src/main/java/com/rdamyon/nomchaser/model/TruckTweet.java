
package com.rdamyon.nomchaser.model;

import java.io.Serializable;

public class TruckTweet implements Serializable
{

	private static final long serialVersionUID = 6454064097265410347L;

	private String text;

    private String retweeted;

    private String entities;  //TODO Needed?

    private String created_at; //Convert to correct timezone?

    public TruckTweet()
    {
    }

    public TruckTweet(final String text, final String retweeted, final String entities, final String created_at)
    {
        this.text = text;
        this.retweeted = retweeted;
        this.entities = entities;
        this.created_at = created_at;
    }

    public String getText()
    {
        return this.text;
    }

    public void setText(final String text)
    {
        this.text = text;
    }

    public String getRetweeted()
    {
        return this.retweeted;
    }

    public void setRetweeted(final String retweeted)
    {
        this.retweeted = retweeted;
    }

    public String getEntities()
    {
        return this.entities;
    }

    public void setEntities(final String entities)
    {
        this.entities = entities;
    }

    public String getCreated_at()
    {
        return this.created_at;
    }

    public void setCreated_at(final String created_at)
    {
        this.created_at = created_at;
    }
}
