package com.rdamyon.nomchaser.model;

/**
 * Created with IntelliJ IDEA.
 * User: ReneNew
 * Date: 27/08/13
 * Time: 9:38 PM
 * To change this template use File | Settings | File Templates.
 */
public enum TruckType{
    FAVOURITE("Favourites", "fav"),
    ADELAIDE("Adelaide","adel"),
    BRISBANE("Brisbane","bris"),
    MELBOURNE("Melbourne","mel"),
    PERTH("Perth","per"),
    SYDNEY("Sydney","syd");

    private String displayName;
    private String tag;
    
    private TruckType(String displayName, String tag){
        this.displayName = displayName;
        this.tag = tag;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }


    @Override
    public String toString() {
        return displayName;
    }

}
