package com.rdamyon.nomchaser.model;

import java.io.Serializable;
import java.util.List;

public class Truck implements Serializable{

	private static final long serialVersionUID = 1388749566309832884L;
	private String twitterId;
	private String city;
	private Boolean favourite;
	private User user;
	private List<TruckTweet> tweets;
	
	public Truck()
	{
		
	}
	
	public Truck(String twitterId, String city, Boolean favourite, User user, List<TruckTweet> tweets)
	{
		this.twitterId = twitterId;
		this.city = city;
		this.favourite = favourite;
		this.user = user;
		this.tweets = tweets;
		
	}

	
	public String getTwitterId() {
		return twitterId;
	}
	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Boolean getFavourite() {
		return favourite;
	}
	public void setFavourite(Boolean favourite) {
		this.favourite = favourite;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<TruckTweet> getTweets() {
		return tweets;
	}

	public void setTweets(List<TruckTweet> tweets) {
		this.tweets = tweets;
	}
}
