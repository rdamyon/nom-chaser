package com.rdamyon.nomchaser.server.restlet;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import com.rdamyon.nomchaser.server.cron.UpdateFoodTruckTimelines;
import com.rdamyon.nomchaser.server.objectify.entity.FoodTruckList;
import com.rdamyon.nomchaser.server.objectify.entity.FoodTruckTimeline;

import com.googlecode.objectify.ObjectifyService;

public class NomChaserApplication extends Application{

	@Override
	public Restlet createInboundRoot() {
				
		this.setup();
        // Create a router Restlet that routes each call
        Router router = new Router(getContext());

        // Defines only one route
        router.attach("/timelines", NomChaserTweetResource.class);
        router.attach("/cron/updateTimelines", UpdateFoodTruckTimelines.class);

        return router;
	}
	
	private void setup()
	{
		// Enable generating raw json from Twitter4J classes
		System.setProperty("jsonStoreEnabled", "true"); 
		ObjectifyService.register(FoodTruckTimeline.class);
		ObjectifyService.register(FoodTruckList.class);

	}
}
