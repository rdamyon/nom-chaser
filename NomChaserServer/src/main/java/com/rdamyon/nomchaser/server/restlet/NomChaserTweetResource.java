package com.rdamyon.nomchaser.server.restlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import org.restlet.representation.Variant;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.rdamyon.nomchaser.server.objectify.entity.FoodTruckTimeline;
import com.rdamyon.nomchaser.server.utils.QueryUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

public class NomChaserTweetResource extends ServerResource {
	
	@Get
	public String getTruckTweets(Variant variant)
	{
		JsonArray truckListJson = new JsonArray();
		Gson gson = new Gson();
		List<Long> truckIdList = QueryUtils.getFoodTruckList();
		if(truckIdList != null)
		{
			// TODO Change this to a batch get, performance probably won't change too much though.
			for(Long truckKey : truckIdList)
			{
				FoodTruckTimeline truckTimeline = ofy().load().type(FoodTruckTimeline.class).id(truckKey).now();
				truckListJson.add(gson.toJsonTree(truckTimeline));
			}
		}
		return truckListJson.toString();
	}
}
