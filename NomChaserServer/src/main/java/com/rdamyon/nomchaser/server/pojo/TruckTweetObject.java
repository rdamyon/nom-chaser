
package com.rdamyon.nomchaser.server.pojo;

import java.io.Serializable;

public class TruckTweetObject implements Serializable
{
	private static final long serialVersionUID = 3723222952374450649L;

	private String text;

    private String retweeted;

    private String created_at; //TODO Convert to correct timezone?

    public TruckTweetObject()
    {
    }

    public TruckTweetObject(final String text, final String retweeted, final String created_at)
    {
        this.text = text;
        this.retweeted = retweeted;
        this.created_at = created_at;
    }

    public String getText()
    {
        return this.text;
    }

    public void setText(final String text)
    {
        this.text = text;
    }

    public String getRetweeted()
    {
        return this.retweeted;
    }

    public void setRetweeted(final String retweeted)
    {
        this.retweeted = retweeted;
    }

    public String getCreated_at()
    {
        return this.created_at;
    }

    public void setCreated_at(final String created_at)
    {
        this.created_at = created_at;
    }
}
