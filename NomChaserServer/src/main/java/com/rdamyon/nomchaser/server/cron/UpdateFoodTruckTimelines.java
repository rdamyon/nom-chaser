package com.rdamyon.nomchaser.server.cron;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.objectify.NotFoundException;

import com.rdamyon.nomchaser.server.objectify.entity.FoodTruckTimeline;
import com.rdamyon.nomchaser.server.pojo.TruckTweetObject;
import com.rdamyon.nomchaser.server.pojo.User;
import com.rdamyon.nomchaser.server.twitter.TwitterQueryService;
import com.rdamyon.nomchaser.server.utils.QueryUtils;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterRuntimeException;
import twitter4j.json.DataObjectFactory;

public class UpdateFoodTruckTimelines extends ServerResource{

	private static final Logger log = Logger.getLogger(UpdateFoodTruckTimelines.class.getName());	 
	
	@Get
	public void updateTimelines()
	{
		List<Long> truckList = QueryUtils.getFoodTruckList();
		if(truckList == null || truckList.size() <= 0)
		{
			QueryUtils.createDefaultFoodTruckList();
			truckList = QueryUtils.getFoodTruckList();
		}
		TwitterQueryService query = new TwitterQueryService();
		if(truckList != null && truckList.size() > 0)
		{
			for(Long truckKey : truckList)
			{
				FoodTruckTimeline truckTimeline = null;
				try{
				
				truckTimeline = ofy().load().type(FoodTruckTimeline.class).id(truckKey).now();
				ResponseList<Status> responses = query.getTruckTimeline(truckTimeline.getTwitterId());
				
				if(responses != null)
				{
					JsonParser jsonParser = new JsonParser();
					List<TruckTweetObject> responseArray = new ArrayList<TruckTweetObject>();
					Gson gson = new Gson();
					User user = null;
					for(Status response : responses)
					{
						String rawJson = DataObjectFactory.getRawJSON(response);
						JsonElement rawJsonElement = jsonParser.parse(rawJson);
						//Parse into a pojo to remove unneeded data.
						TruckTweetObject tweet = gson.fromJson(rawJson, TruckTweetObject.class);
						if(user == null && rawJsonElement.isJsonObject())
						{
							JsonObject rawJsonObject = rawJsonElement.getAsJsonObject();
							if(rawJsonObject.has("user"))
							{
								user = gson.fromJson(rawJsonObject.get("user"), User.class);
							}
							
						}
						//Spit the json back out after parsing
						responseArray.add(tweet);
					}
					truckTimeline.setTweets(responseArray);
					if(user != null)
					{
						truckTimeline.setUser(user);
					}
					ofy().save().entity(truckTimeline).now();
					log.info("Updated " + truckTimeline.getTwitterId() + " successfully");
				}
				}catch(NotFoundException e)
				{
					log.warning("Database entry for key : " + truckKey + " not found. Could not update timeline");
				}catch(TwitterException e)
				{
					log.warning("Status Code : " + e.getStatusCode() + e.getMessage());
				}catch (TwitterRuntimeException e)
				{
					log.warning("TwitterRuntimeException - ID : " + truckTimeline.getTwitterId() + " Message : " + e.getMessage());
				}
			}
		}else
		{
			log.warning("No trucks found");
		}
		
	}
}
