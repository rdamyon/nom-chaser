package com.rdamyon.nomchaser.server.objectify.entity;

import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.rdamyon.nomchaser.server.pojo.TruckTweetObject;
import com.rdamyon.nomchaser.server.pojo.User;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Serialize;

@Cache
@Entity
public class FoodTruckTimeline {

	@Id Long id;
	String twitterId;
	String city;
	@Serialize
	List<TruckTweetObject> tweets;
	@Serialize
	User user;
	
	private FoodTruckTimeline() {}
	
	public FoodTruckTimeline(String twitterId, String city, List<TruckTweetObject> tweets, User user)
	{
		this.twitterId = twitterId;
		this.city = city;
		this.tweets = tweets;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<TruckTweetObject> getTweets() {
		return tweets;
	}

	public void setTweets(List<TruckTweetObject> tweets) {
		this.tweets = tweets;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
