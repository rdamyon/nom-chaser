package com.rdamyon.nomchaser.server.objectify.entity;

import java.util.List;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Cache
@Entity
public class FoodTruckList {

	@Id long id;
	List<Long> truckList;
	
	public FoodTruckList() {}
	
	public FoodTruckList(long id, List<Long> truckList)
	{
		this.id = id;
		this.truckList = truckList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Long> getTruckList() {
		return truckList;
	}

	public void setTruckList(List<Long> truckList) {
		this.truckList = truckList;
	}
	
}
