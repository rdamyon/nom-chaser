package com.rdamyon.nomchaser.server.utils;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.cmd.Query;
import com.rdamyon.nomchaser.server.objectify.entity.FoodTruckList;
import com.rdamyon.nomchaser.server.objectify.entity.FoodTruckTimeline;

public class QueryUtils {

	public static List<Long> getFoodTruckList()
	{
		List<Long> truckList;
		FoodTruckList truckListEntity;

        truckListEntity = ofy().load().type(FoodTruckList.class).id(1).now();
        if(truckListEntity != null){
            truckList = truckListEntity.getTruckList();
        }else{
            //populate list as it hasn't been created or has been cleared.
            truckList = new ArrayList<Long>();

            Query<FoodTruckTimeline> keyList = ofy().load().type(FoodTruckTimeline.class);
            for(FoodTruckTimeline truck : keyList)
            {
                truckList.add(truck.getId());
            }
            // put the list in the datastore for later use. Always use ID of 1
            if(truckList.size() > 0)
            {
                truckListEntity = new FoodTruckList(1, truckList);
                ofy().save().entity(truckListEntity).now();
            }
        }
		
		return truckList;
	}
	
	/**
	 * This should only be used for testing where there is no data in the database. 
	 */
	public static void createDefaultFoodTruckList()
	{
		FoodTruckTimeline testTruck1 = new FoodTruckTimeline("tacotruckmelb", "mel", null, null);
		FoodTruckTimeline testTruck2 = new FoodTruckTimeline("gumbokitchen", "mel", null, null);
		FoodTruckTimeline testTruck3 = new FoodTruckTimeline("smokinbarrys", "mel", null, null);
        ofy().save().entity(testTruck1);
        ofy().save().entity(testTruck2);
        ofy().save().entity(testTruck3);
	}
}
