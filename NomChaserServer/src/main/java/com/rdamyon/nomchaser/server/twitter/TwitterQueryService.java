package com.rdamyon.nomchaser.server.twitter;

import java.util.logging.Logger;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterRuntimeException;
import twitter4j.auth.AccessToken;

public class TwitterQueryService {

	private static final Logger log = Logger.getLogger(TwitterQueryService.class.getName());	

	//TODO Move to a properties file (also shouldn't be in git repo really...)
	private static final String consumerKey = "5pfVXUb7WEL2Xz7JEBw6kw";
	private static final String consumerSecret = "M0FCqsefTHXGryeaBOJGLX7ZlIGyB3ziUVQvkSU8ukU";
	private static final String accessTokenName = "747129020-MWd5Akz2BB6QV9Fzn37Z9n7fFhWpv9B9UV0wuaqC";
	private static final String accessTokenSecret = "GIRODduPr0Q6DOAqpCrZFYIFcJODaKv7bZObgd90";
	
	public ResponseList<Status> getTruckTimeline(String id) throws TwitterException
	{
		ResponseList<Status> responses = null;
		
		Twitter twit = new TwitterFactory().getInstance();
		twit.setOAuthConsumer(consumerKey, consumerSecret);
		AccessToken accessToken = new AccessToken(accessTokenName, accessTokenSecret);
		twit.setOAuthAccessToken(accessToken);
		
		Paging paging = new Paging();
		paging.setCount(20);
		try {
			responses = twit.getUserTimeline(id, paging);
		} catch (TwitterException e) {
			log.warning("TwitterException - ID : " + id + " Status Code : " + e.getStatusCode() + e.getMessage());
		}catch (TwitterRuntimeException e)
		{
			log.warning("TwitterRuntimeException - ID : " + id + " Message : " + e.getMessage());
		}
		
		return responses;
	}
}
